using System;
using UnityEngine;
using UnityEngine.UI;

namespace UIScreens
{
    public class MainScreen : UIScreen
    {
        [SerializeField]
        private Button startButton;

        public void BindButton(Action startButtonAction)
        {
            startButton.onClick.AddListener(() => startButtonAction?.Invoke());
        }
    }
}