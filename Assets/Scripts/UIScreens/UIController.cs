using Common;
using GameEvents;
using UnityEngine;

namespace UIScreens
{
    public class UIController : MonoBehaviour,
        IGameReadyListener,
        IGameStartListener,
        IGamePauseListener,
        IGameResumeListener,
        IGameFinishListener
    {
        [SerializeField] 
        private MainScreen mainScreen;
        
        [SerializeField]
        private TimerScreen timerScreen;
        
        [SerializeField]
        private GameScreen gameScreen;

        [SerializeField] 
        private FinishScreen finishScreen;

        private GameManager _gameManager;
        private Timer _timer;

        public void Init(GameManager gameManager, Timer timer)
        {
            mainScreen.BindButton(gameManager.ReadyGame);
            gameScreen.BindButton(OnPauseBtnClicked);

            _gameManager = gameManager;
            _timer = timer;
        }

        void IGameReadyListener.OnGameReady()
        {
            mainScreen.Hide();
            
            timerScreen.SetTimerText(_timer.TimerSeconds);
            _timer.OnTimerTick += timerScreen.SetTimerText;
            
            timerScreen.Show();
        }

        void IGameStartListener.OnGameStart()
        {
            timerScreen.Hide();
            gameScreen.Show();
            
            _timer.OnTimerTick -= timerScreen.SetTimerText;
        }

        void IGamePauseListener.OnGamePause()
        {
            gameScreen.ToggleScreenState(true);
        }

        void IGameResumeListener.OnGameResume()
        {
            gameScreen.ToggleScreenState(false);
        }

        void IGameFinishListener.OnGameFinish()
        {
            gameScreen.Hide();
            finishScreen.Show();
        }

        private void OnPauseBtnClicked()
        {
            if(_gameManager.State == GameStates.Paused)
                _gameManager.ResumeGame();
            else 
                _gameManager.PauseGame();
        }
    }
}