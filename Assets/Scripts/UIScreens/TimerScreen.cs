using TMPro;
using UnityEngine;

namespace UIScreens
{
    public class TimerScreen : UIScreen
    {
        [SerializeField]
        private TMP_Text timerText;

        public void SetTimerText(int timerValue)
        {
            timerText.text = timerValue.ToString();
        }
    }
}