using UnityEngine;

namespace UIScreens
{
    public abstract class UIScreen : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}