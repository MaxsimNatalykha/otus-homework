using System;
using UnityEngine;
using UnityEngine.UI;

namespace UIScreens
{
    public class GameScreen : UIScreen
    {
        [SerializeField]
        private Button pauseButton;

        [SerializeField] 
        private Image pauseBtnImage;
        
        [SerializeField] 
        private Sprite pauseIcon, resumeIcon;

        [SerializeField] 
        private RectTransform controlPanel;

        public void BindButton(Action onPauseBtnClicked)
        {
            pauseButton.onClick.AddListener(() => onPauseBtnClicked?.Invoke());
        }

        public void ToggleScreenState(bool isPause)
        {
            pauseBtnImage.sprite = isPause ? resumeIcon : pauseIcon;
            controlPanel.gameObject.SetActive(!isPause);
        }
    }
}