using UnityEngine;

namespace GameEvents
{
    public class GameManagerInstaller : MonoBehaviour
    {
        private void Awake()
        {
            var gameManager = GetComponentInChildren<GameManager>();
            var gameListener = GetComponentsInChildren<IGameListener>();

            foreach (var listener in gameListener)
                gameManager.AddGameListener(listener);
        }
    }
}