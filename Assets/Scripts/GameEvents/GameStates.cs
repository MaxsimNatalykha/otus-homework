namespace GameEvents
{
    public enum GameStates
    {
        Off,
        Ready,
        Playing,
        Paused,
        Finished
    }
}