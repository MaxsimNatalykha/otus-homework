namespace GameEvents
{
    public interface IGameListener
    {
        
    }

    public interface IGameReadyListener : IGameListener
    {
        void OnGameReady();
    }

    public interface IGameStartListener : IGameListener
    {
        void OnGameStart();
    }
    
    public interface IGamePauseListener : IGameListener
    {
        void OnGamePause();
    }
    
    public interface IGameResumeListener : IGameListener
    {
        void OnGameResume();
    }
    
    public interface IGameFinishListener : IGameListener
    {
        void OnGameFinish();
    }

    public interface IUpdateListener : IGameListener
    {
        void OnUpdate(float deltaTime);
    }    
    
    public interface IRealtimeUpdateListener : IGameListener
    {
        void OnUpdate(float deltaTime);
    }
}