using System.Collections.Generic;
using UnityEngine;

namespace GameEvents
{
    public class GameManager : MonoBehaviour
    {
        public GameStates State {get {return _state;}}
            
        private readonly List<IGameListener> _gameListeners = new ();
        private readonly List<IUpdateListener> _updateListeners = new ();
        private readonly List<IRealtimeUpdateListener> _realtimeUpdateListeners = new ();

        private GameStates _state = GameStates.Off;

        public void AddGameListener(IGameListener listener)
        {
            if (listener == null) return;
            
            _gameListeners.Add(listener);

            if (listener is IUpdateListener updateListener)
                _updateListeners.Add(updateListener);
            
            if (listener is IRealtimeUpdateListener realtimeUpdateListener)
                _realtimeUpdateListeners.Add(realtimeUpdateListener);
        }

        public void ReadyGame()
        {
            foreach (var gameListener in _gameListeners)
            {
                if(gameListener is IGameReadyListener listener)
                    listener.OnGameReady();
            }

            _state = GameStates.Ready;
            
            Debug.Log("Ready");
        }

        public void StartGame()
        {
            foreach (var gameListener in _gameListeners)
            {
                if(gameListener is IGameStartListener listener)
                    listener.OnGameStart();
            }

            _state = GameStates.Playing;
            
            Debug.Log("Start");
        }

        public void PauseGame()
        {
            foreach (var gameListener in _gameListeners)
            {
                if(gameListener is IGamePauseListener listener)
                    listener.OnGamePause();
            }
            
            _state = GameStates.Paused;
            
            Debug.Log("Pause");
        }        
        
        public void ResumeGame()
        {
            foreach (var gameListener in _gameListeners)
            {
                if(gameListener is IGameResumeListener listener)
                    listener.OnGameResume();
            }
            
            _state = GameStates.Playing;
            
            Debug.Log("Resume");
        }        
        
        public void FinishGame()
        {
            foreach (var gameListener in _gameListeners)
            {
                if(gameListener is IGameFinishListener listener)
                    listener.OnGameFinish();
            }
            
            _state = GameStates.Finished;
            
            Debug.Log("Finish");
        }

        private void Update()
        {
            if (State == GameStates.Playing)
            {
                foreach (var updateListener in _updateListeners)
                {
                    updateListener.OnUpdate(Time.deltaTime);
                }
            }

            foreach (var realtimeUpdateListener in _realtimeUpdateListeners)
            {
                realtimeUpdateListener.OnUpdate(Time.deltaTime);
            }
        }
    }
}