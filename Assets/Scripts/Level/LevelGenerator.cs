using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class LevelGenerator : MonoBehaviour
    {
        public List<RoadCluster> SpawnedRoad => _spawnedRoad;
        public List<Obstacle> SpawnedObstacles => _spawnedObstacle;

        [SerializeField]
        private int roadLength;

        [SerializeField]
        private int obstacleAmount;

        [SerializeField] 
        private GameObject roadPrefab, obstaclePrefab;

        [SerializeField] 
        private Transform roadContainer, obstacleContainer, showPosition, hidePosition;

        private List<RoadCluster> _spawnedRoad = new();
        private List<Obstacle> _spawnedObstacle = new();
        
        private const int ROAD_SIZE = 20;
        private const int OBSTACLE_SIZE = 5;

        public void GenerateLevel()
        {
            SpawnLevelObjects();
            InitializeRoad();
            InitializeObstacles();
        }

        private void SpawnLevelObjects()
        {
            var roadSpawner = new LevelObjectSpawner<RoadCluster>(roadPrefab, roadContainer);
            _spawnedRoad = roadSpawner.Spawn(roadLength, showPosition, hidePosition);

            var obstacleSpawner = new LevelObjectSpawner<Obstacle>(obstaclePrefab, obstacleContainer);
            _spawnedObstacle = obstacleSpawner.Spawn(obstacleAmount, showPosition, hidePosition);
        }

        private void InitializeRoad()
        {
            for (int i = 0; i < _spawnedRoad.Count; i++)
            {
                _spawnedRoad[i].transform.position = new Vector3(0, 0, i * ROAD_SIZE);
            }
        }

        private void InitializeObstacles()
        {
            for (int i = 0; i < _spawnedObstacle.Count; i++)
            {
                int xPos = Random.Range(-1, 2) * OBSTACLE_SIZE;
                float zPos = Random.Range(20f, 140f);

                _spawnedObstacle[i].transform.position = new Vector3(xPos, 0, zPos);
            }
        }
    }
}