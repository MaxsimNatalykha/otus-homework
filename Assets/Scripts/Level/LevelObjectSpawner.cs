using System.Collections.Generic;
using Common;
using UnityEngine;

namespace Level
{
    public class LevelObjectSpawner<TObject> where TObject : LevelObject
    {
        private readonly PrefabFactory<TObject> _factory;
        
        public LevelObjectSpawner(GameObject prefab, Transform container)
        {
            _factory = new PrefabFactory<TObject>(prefab, container);
        }

        public List<TObject> Spawn(int amount, Transform showPosition, Transform hidePosition)
        {
            var spawnedObjects = new List<TObject>();
            
            for (int i = 0; i < amount; i++)
            {
                var spawnedObject = _factory.Create();
                spawnedObject.Setup(showPosition.position, hidePosition.position);
                spawnedObjects.Add(spawnedObject);
            }

            return spawnedObjects;
        }
    }
}