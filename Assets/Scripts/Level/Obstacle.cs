using UnityEngine;

namespace Level
{
    public class Obstacle : LevelObject
    {
        private const int OBSTACLE_SIZE = 5;

        protected override void Reset()
        {
            float xPos = Random.Range(-1, 2) * OBSTACLE_SIZE;
            float zPos = Random.Range(-20, 20) + _showPosition;

            transform.position = new Vector3(xPos, 0, zPos);
        }
    }
}