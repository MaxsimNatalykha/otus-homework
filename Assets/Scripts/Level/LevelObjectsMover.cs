using System.Collections.Generic;
using GameEvents;
using UnityEngine;

namespace Level
{
    public class LevelObjectsMover : MonoBehaviour, IUpdateListener
    {
        [SerializeField]
        private float speed;
        
        private readonly List<LevelObject> _levelObjects = new();

        public void AddLevelObjects(IEnumerable<LevelObject> levelObjects)
        {
            _levelObjects.AddRange(levelObjects);
        }

        public void OnUpdate(float deltaTime)
        {
            var newPosition = Vector3.back * speed * deltaTime;
            
            for (int i = 0; i < _levelObjects.Count; i++)
            {
                _levelObjects[i].Move(newPosition);
            }
        }
    }
}