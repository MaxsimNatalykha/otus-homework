using UnityEngine;

namespace Level
{
    public abstract class LevelObject : MonoBehaviour
    {
        protected float _showPosition;
        protected float _hidePosition;
        
        public void Setup(Vector3 showPosition, Vector3 hidePosition)
        {
            _showPosition = showPosition.z;
            _hidePosition = hidePosition.z;
        }

        public void Move(Vector3 delta)
        {
            transform.Translate(delta);
            
            if (transform.position.z <= _hidePosition)
                Reset();
        }

        protected virtual void Reset()
        {
            transform.position = new Vector3(0,0,_showPosition);
        }
    }
}