using GameEvents;
using PlayerScripts;
using UnityEngine;

namespace Controllers
{
    public class MoveController : 
        IGameStartListener, 
        IGameFinishListener
    {
        private readonly IMoveInput _input;
        private readonly IControllable _controllable;

        private const float MOVE_OFFSET = 5;
        
        private const int MIN_X_POS = -5;
        private const int MAX_X_POS = 5;

        public MoveController(IMoveInput input, IControllable controllable)
        {
            _input = input;
            _controllable = controllable;
        }
        
        void IGameStartListener.OnGameStart()
        {
            _input.OnMove += OnMove;
        }

        void IGameFinishListener.OnGameFinish()
        {
            _input.OnMove -= OnMove;
        }

        private void OnMove(Vector2 direction)
        {
            float xPosDelta = direction.x * MOVE_OFFSET;

            if (CanMove(xPosDelta))
                _controllable.Move(new Vector3(xPosDelta, 0, 0));
        }

        private bool CanMove(float xPosDelta)
        {
            return !(_controllable.GetPosition().x + xPosDelta < MIN_X_POS || _controllable.GetPosition().x + xPosDelta > MAX_X_POS);
        }
    }
}