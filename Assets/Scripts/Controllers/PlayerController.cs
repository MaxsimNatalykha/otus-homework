using GameEvents;
using PlayerScripts;

namespace Controllers
{
    public class PlayerController : 
        IGameStartListener, 
        IGameFinishListener
    {
        private readonly Player _player;
        private readonly GameManager _gameManager;

        public PlayerController(Player player, GameManager gameManager)
        {
            _player = player;
            _gameManager = gameManager;
        }
        
        void IGameStartListener.OnGameStart()
        {
            _player.OnHitAction += _gameManager.FinishGame;
        }

        void IGameFinishListener.OnGameFinish()
        {
            _player.OnHitAction -= _gameManager.FinishGame;
        }
    }
}