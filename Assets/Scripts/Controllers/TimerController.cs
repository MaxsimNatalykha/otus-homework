using Common;
using GameEvents;

namespace Controllers
{
    public class TimerController :
        IGameReadyListener,
        IGameStartListener
    {
        private readonly GameManager _gameManager;
        private readonly Timer _timer;

        public TimerController(GameManager gameManager, Timer timer)
        {
            _gameManager = gameManager;
            _timer = timer;
        }
        
        void IGameReadyListener.OnGameReady()
        {
            _timer.OnTimerLeft += _gameManager.StartGame;
        }

        void IGameStartListener.OnGameStart()
        {
            _timer.OnTimerLeft -= _gameManager.StartGame;
        }
    }
}