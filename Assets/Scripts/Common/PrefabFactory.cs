using UnityEngine;

namespace Common
{
    public class PrefabFactory<TPrefab>
    {
        private readonly GameObject _prefab;
        private readonly Transform _container;

        public PrefabFactory(GameObject prefab, Transform container)
        {
            _prefab = prefab;
            _container = container;
        }

        public TPrefab Create()
        {
            var instantiateObject = Object.Instantiate(_prefab, _container);
            return instantiateObject.GetComponent<TPrefab>();
        }
    }
}