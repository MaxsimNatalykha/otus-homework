using System;
using GameEvents;
using UnityEngine;

namespace Common
{
    public class Timer : 
        MonoBehaviour, 
        IRealtimeUpdateListener, 
        IGameReadyListener
    {
        public int TimerSeconds => timerSeconds;

        public Action<int> OnTimerTick;
        public Action OnTimerLeft;

        [SerializeField] 
        private int timerSeconds;

        private bool _active;
        private float _timer;

        void IGameReadyListener.OnGameReady()
        {
            _active = true;
        }

        void IRealtimeUpdateListener.OnUpdate(float deltaTime)
        {
            if (!_active) return;
            
            _timer += deltaTime;

            if (_timer >= 1f)
                TimerSecondTick();

            if (timerSeconds <= 0)
                TimerLeft();
        }

        private void TimerSecondTick()
        {
            _timer = 0;
            timerSeconds--;
            OnTimerTick?.Invoke(TimerSeconds);
        }

        private void TimerLeft()
        {
            _active = false;
            _timer = 0;
            OnTimerLeft?.Invoke();
        }
    }
}