using Controllers;
using GameEvents;
using Level;
using PlayerScripts;
using UIScreens;
using UnityEngine;

namespace Common
{
    public class EntryPoint : MonoBehaviour
    {
        [SerializeField]
        private GameManager gameManager;

        [SerializeField] 
        private Player player;

        [SerializeField] 
        private UIInput uiInput;

        [SerializeField] 
        private UIController uiController;

        [SerializeField] 
        private LevelGenerator levelGenerator;

        [SerializeField] 
        private LevelObjectsMover levelObjectsMover;

        [SerializeField] 
        private Timer timer;

        private void Awake()
        {
            InitializeGame();
        }

        private void InitializeGame()
        {
            var moveController = new MoveController(uiInput, player);
            var timerController = new TimerController(gameManager, timer);
            var playerController = new PlayerController(player, gameManager);

            gameManager.AddGameListener(moveController);
            gameManager.AddGameListener(timerController);
            gameManager.AddGameListener(playerController);
            gameManager.AddGameListener(uiInput);

            levelGenerator.GenerateLevel();
        
            levelObjectsMover.AddLevelObjects(levelGenerator.SpawnedRoad);
            levelObjectsMover.AddLevelObjects(levelGenerator.SpawnedObstacles);

            uiController.Init(gameManager, timer);
        }
    }
}