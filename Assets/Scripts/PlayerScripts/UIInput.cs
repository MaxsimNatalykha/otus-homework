using System;
using GameEvents;
using UnityEngine;
using UnityEngine.UI;

namespace PlayerScripts
{
    public class UIInput : MonoBehaviour, 
        IGameStartListener, 
        IGameFinishListener, 
        IMoveInput
    {
        public Action<Vector2> OnMove { get; set; }

        [SerializeField] 
        private Button leftBtn, rightBtn;

        void IGameStartListener.OnGameStart()
        {
            leftBtn.onClick.AddListener(MoveLeft);
            rightBtn.onClick.AddListener(MoveRight);
        }

        void IGameFinishListener.OnGameFinish()
        {
            leftBtn.onClick.RemoveListener(MoveLeft);
            rightBtn.onClick.RemoveListener(MoveRight);
        }

        private void MoveLeft()
        {
            OnMove?.Invoke(Vector2.left);
        }
        
        private void MoveRight()
        {
            OnMove?.Invoke(Vector2.right);
        }
    }
}