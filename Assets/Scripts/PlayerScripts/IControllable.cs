using UnityEngine;

namespace PlayerScripts
{
    public interface IControllable
    {
        void Move(Vector3 delta);
        Vector3 GetPosition();
    }
}