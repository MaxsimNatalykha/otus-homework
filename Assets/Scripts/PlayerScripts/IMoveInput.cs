using System;
using UnityEngine;

namespace PlayerScripts
{
    public interface IMoveInput
    { 
        Action<Vector2> OnMove { get; set; }
    }
}