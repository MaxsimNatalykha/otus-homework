using System;
using Level;
using UnityEngine;

namespace PlayerScripts
{
    public class Player : MonoBehaviour, IControllable
    {
        public Action OnHitAction { get; set; }

        void IControllable.Move(Vector3 delta)
        {
            transform.position += delta;
        }

        Vector3 IControllable.GetPosition()
        {
            return transform.position;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponentInParent<Obstacle>())
                OnHitAction?.Invoke();
        }
    }
}